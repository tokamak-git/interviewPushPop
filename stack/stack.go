package stack

// package stack
// type stack []int

// Push stack
func Push(s []int, i int) []int {
	return append(s, i)
}

// Pop stack
func Pop(s []int) []int {
	return s[:len(s)-1]
}

// push
// func (s *stack) push(i int) {
//	append(s, i)
// }
//
// // pop
// func (s *stack) pop() {
//	s[:len(s)-1]
// }
