package stack

import (
	"fmt"
	"log"
	"reflect"
	"testing"
)

// test cases
type testCase struct {
	input       []int
	expectedOut []int
}

const length = 5

var (
	testStack    = []int{1, 2, 3, 4}
	pushTestCase = testCase{

		// push test
		input:       testStack,
		expectedOut: []int{1, 2, 3, 4, 5},
	}

	popTestCase = testCase{
		// pop test
		input:       testStack,
		expectedOut: []int{1, 2, 3},
	}
)

// running test
// TestPush test
func TestPush(t *testing.T) {
	result := Push(pushTestCase.input, 5)
	if reflect.DeepEqual(result, pushTestCase.expectedOut) {
		log.Println("Push Test Successfull")
	} else {
		t.Errorf("return: %v", pushTestCase.input)
		t.Fatal("failed push test")
	}
}

// TestPop test
func TestPop(t *testing.T) {
	result := Pop(popTestCase.input)
	if reflect.DeepEqual(result, popTestCase.expectedOut) {
		fmt.Println("Pop Test Successfull")
	} else {
		t.Fatal("failed pop test")
	}

	// handle boundry conditions
	// testable code
	// export push and pop
}
