package main // ckage main

import (
	"fmt"
	"interview/stack"
)

func main() {
	// initilize a length
	const length = 5
	// int type

	// test stack
	newStack := []int{1, 2, 3, 4}
	newStack = stack.Push(newStack, 5)
	fmt.Println("Push:", newStack)
	fmt.Println("Pop:", stack.Pop(newStack))
}
